# BiteFight bot
User will login (he has to be registered on czech bitefight server Medias), then he is redirected to official bitefight website, where is automatically loged in.
His creature will go hunt (until energy is exhausted), then It will invest earned golds to abilities and after that, bot will go to job for 2 hours and the whole process repeats. 

<img src="./bf-bot.png">

# Development
bot.py script contains a whole code. If you want to run/modify it on your computer, you should have installed: Python 3, selenium with geckodriver (mozilla), tkinter and PIL. I did not share background image, so you will have to set background as some color or use your own image. The next thing You will need is open account on medias server (bitefight.cz). I did not share the script on gitlab, because I don't want to show site weaknesses, so the script is used for study purposes only and It is not publicly shared.

# Video
https://youtu.be/-lCCP_oZ7E8

# Link
https://uloz.to/file/0VX4aQtRF1Jm/bitefightbot-zip
